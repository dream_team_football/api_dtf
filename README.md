# API Dream Team Football

API desarrollada por el Equipo de DAM:
- [Alejandro Torres](https://gitlab.com/alejandro.torres.7e6)
- [Gerard Sanchez](https://gitlab.com/gerard.sanchez.7e6)
- [Marçal Herraiz](https://gitlab.com/marcal.herraiz.7e5)
- [Jordi Robles](https://gitlab.com/Jordiii5)

## Descripción
La API Dream Team Football proporciona los servicios necesarios para la gestión de equipos mixtos de fútbol. Permite a los usuarios acceder a funcionalidades como la creación de equipos mixtos, el mercado de fichajes y la clasificación de jugadores.

## Características principales
- Creación de Equipos Mixtos: Los usuarios pueden crear y gestionar sus propios equipos de fútbol mixto, seleccionando jugadores de ambos géneros.
- Mercado de Fichajes: Acceso a un mercado de fichajes donde los usuarios pueden buscar, comprar y vender jugadores para fortalecer sus equipos.
- Clasificación: Los usuarios pueden ver su desempeño y posición en una clasificación basada en el rendimiento de los jugadores alineados durante las jornadas.

## Tecnologías utilizadas
- Lenguaje de Programación: [Kotlin](https://kotlinlang.org/)
- Framework: [Ktor](https://ktor.io/)
- Base de Datos: [PostgreSQL](https://www.postgresql.org/)

## Endpoints
- `/estadisticasJugadorRoute`: Gestión de las estadisticas de cada jugador.
- `/JugadorRoute`: Gestion de todos los jugadores.
- `/LoginRegisterRoute`: Gestion del inicio de sesión y del register.
- `/UsuarioRoute`: Gestion de los usuarios que tiene la app.

## Licencia
[MIT License](LICENSE)

# BackEnd
[Acceso al BackEnd](https://gitlab.com/dream_team_football/api_dtf)

# BBDD
[Acceso a la Base de datos](https://api.elephantsql.com/console/e6cf8987-1b5f-424f-a20f-b12996b58eb1/browser?#)
