package com.example.model

import com.example.dao.DAOEstadisticasJugador
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

@Serializable
data class EstadisticaJugador(
    val idJornada: Int,
    val idJugador: Int,
    val idEstadistica: Int,
    val minutos: Int?,
    val goles: Int?,
    val asistencias: Int?,
    val paradas: Int?,
    val tirosAPuerta: Int?,
    val balonesPerdidos: Int?,
    val valoracion: Int?
)


object EstadisticasJugador: Table("estadisticasjugador"){
    val idJornada= integer("idjornada")
    val idJugador= integer("idjugador")
    val idEstadistica= integer("idestadistica").autoIncrement()
    val minutos= integer("minutos")
    val goles= integer("goles")
    val asistencias= integer("asistencias")
    val paradas= integer("paradas")
    val tirosAPuerta= integer("tirosapuerta")
    val balonesPerdidos= integer("balonesperdidos")
    val valoracion= integer("valoracion")
    override val primaryKey = PrimaryKey(idEstadistica)
}