package com.example.model

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.BooleanColumnType

@Serializable
data class Jugador(
    val idJugador: Int,
    val idUsuario: Int?,
    val nombre: String,
    val shortName: String,
    val liga: String,
    val equipo: String,
    val posicion: String,
    val alineado: Boolean,
    val precio: Int,
    val precioClausula: Int,
    val puntosTotales: Int,
    val imagenUrl: String
)

object Jugadores: Table("jugador"){
    val idJugador= integer("idjugador")
    val idUsuario = integer("idusuario").nullable()
    val nombre= varchar("nombre", 255)
    val shortName= varchar("shortname", 50)
    val liga= varchar("liga", 50)
    val equipo= varchar("equipo", 255)
    val posicion= varchar("posicion", 255)
    val alineado =  bool("alineado")
    val precio= integer("precio")
    val precioClausula= integer("precioclausula")
    val puntosTotales= integer("puntostotales")
    val imagenUrl= varchar("imagenurl", 255)

    override val primaryKey = PrimaryKey(idJugador)
}

//    suspend fun descargarImagenes() {
//        val listaJugadores = dao.allJugadores() // Asegúrate de que el método dao.allJugadores() devuelva la lista de jugadores desde la base de datos
//
//        for (jugador in listaJugadores) {
//            val imageUrl = jugador.imagenUrl
//            val imageName = jugador.nombre + ".jpg"
//
//            try {
//                val url = URL(imageUrl)
//                val imageBytes = url.readBytes()
//                val file = File("./images/$imageName")
//                file.writeBytes(imageBytes)
//                println("Imagen de ${jugador.nombre} descargada y guardada como $imageName")
//            } catch (e: Exception) {
//                println("Error al descargar la imagen para ${jugador.nombre}: ${e.message}")
//            }
//        }
//    }


