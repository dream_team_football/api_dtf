package com.example.model

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

@Serializable
data class Usuario(
    val usu_id: Int,
    val usu_username: String,
    val usu_password: String,
    val puntosTotales: Int,
    val dinero: Int

)

object Usuarios: Table("usuario"){
    val usu_id= integer("idusuario").autoIncrement()
    val usu_username = varchar("nombre",255)
    val usu_password = varchar("password",255)
    val puntosTotales= integer("puntostotales")
    val dinero = integer("dinero")
}