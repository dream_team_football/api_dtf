package com.example.routes

import com.example.dao.dao
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.io.File

fun Route.jugadorRouting(){
    route("/jugadores") {
        get {
            val jugadorList = dao.allJugadores()
            if (jugadorList.isNotEmpty()) {
                call.respond(jugadorList)
            } else {
                call.respondText("No hay ningún jugador en la base de datos", status = HttpStatusCode.OK)
            }
        }
        get("/{liga}/{equipo}/{posicion}"){
            val liga: String
            val equipo: String
            val posicion: String

            try {
                liga = call.parameters["liga"]!!
                equipo = call.parameters["equipo"]!!
                posicion = call.parameters["posicion"]!!

                val jugadoresFiltrados = dao.allJugadores().filter { jugador ->
                    (liga.isBlank() || liga == "Liga" || jugador.liga == liga) &&
                            (equipo.isBlank() || equipo == "Equipo" || jugador.equipo == equipo) &&
                            (posicion.isBlank() || posicion == "POSICIÓN" || jugador.posicion == posicion)
                }

                if (jugadoresFiltrados.isNotEmpty()) {
                    call.respond(jugadoresFiltrados)
                } else {
                    call.respondText("No se encontraron jugadores con los parámetros proporcionados.", status = HttpStatusCode.NotFound)
                }


            }catch (e: InvalidBodyException){
                call.respondText("[ERROR] en la ruta.", status = HttpStatusCode.BadRequest)
            }

        }

        get("/{idUsuario}") {

            val idUsuario: Int

            try {
                idUsuario = call.parameters["idUsuario"]!!.toInt()

                val jugadoresEquipo = dao.allJugadores().filter { it.idUsuario == idUsuario }
                call.respond(jugadoresEquipo)

            }catch (e: InvalidBodyException){
                call.respondText("[ERROR] en la ruta.", status = HttpStatusCode.BadRequest)
            }

        }

        get("/imagenes/{imageName}"){
            val imageName = call.parameters["imageName"]
            val file = File("./images/$imageName")
            println(file)
            if (file.exists()) {
                call.respondFile(File("./images/$imageName"))
            } else {
                call.respondText("Imagen no encontrada.", status = HttpStatusCode.NotFound)
            }
        }

        put("/idJugador/{idJugador}/{idUsuario}") {
            val jugadorId: Int
            val usuId: Int?

            try {
                jugadorId = call.parameters["idJugador"]!!.toInt()
                usuId = call.parameters["idUsuario"]?.toInt()

                if (dao.updateUsuIdJugador(jugadorId, usuId)) {
                    call.respondText("S'ha cambiat correctament", status = HttpStatusCode.OK)
                } else {
                    call.respondText("No s'ha pogut fer l'update", status = HttpStatusCode.InternalServerError)
                }

            } catch (e: NumberFormatException) {
                call.respondText("[ERROR] en el parametre.", status = HttpStatusCode.BadRequest)
            }
        }

        put("/updatealineado/{idJugador}/{alineado}"){

            val idJugador: Int
            val alineado: Boolean

            try {
                idJugador = call.parameters["idJugador"]!!.toInt()
                alineado = call.parameters["alineado"]!!.toBoolean()

                if (dao.updateJugadorAlineado(idJugador, alineado)){
                    call.respondText("S'ha cambiat correctament", status = HttpStatusCode.OK)
                }else {
                    call.respondText("No s'ha pogut fer l'update", status = HttpStatusCode.InternalServerError)
                }

            }catch (e: NumberFormatException){
                call.respondText("[ERROR] en el parametre.", status = HttpStatusCode.BadRequest)
            }
        }


    }
}
