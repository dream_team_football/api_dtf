package com.example.routes

import com.example.dao.daoUsuario
import com.example.model.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.LoginRegister(){
    route("user"){
        post("/register") {
            val user = call.receive<Usuario>()

            println("Nuevo usuario recibido: $user")

            val usersFromDB = uploadUser()
            if (usersFromDB.containsKey(user.usu_username)) {
                call.respondText("El usuario ya existe", status = HttpStatusCode.Conflict)
                return@post
            } else {
                daoUsuario.addNewUsuario(user.usu_username, user.usu_password)
                userTable[user.usu_username] = getMd5Digest("${user.usu_username}:$myRealm:${user.usu_password}")
                call.respondText("Usuario registrado correctamente", status = HttpStatusCode.Accepted)
            }
        }
        post("/login") {
            val user = call.receive<Usuario>()
            userTable=uploadUser()
            val userHidden = getMd5Digest("${user.usu_username}:$myRealm:${user.usu_password}")
            if (userTable.containsKey(user.usu_username) && userTable[user.usu_username]?.contentEquals(userHidden) == true) {
                call.respondText("Login correcte", status = HttpStatusCode.Accepted)
                return@post
            }
            else {
                call.respondText("Login incorrecte", status = HttpStatusCode.Conflict)
            }

        }


    }

}

