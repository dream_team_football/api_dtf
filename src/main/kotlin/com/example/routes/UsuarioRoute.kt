package com.example.routes

import com.example.dao.dao
import com.example.dao.daoUsuario
import com.example.model.Usuario
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*


fun Route.usuarioRoute(){
    authenticate("myAuth"){
        route("/usuario"){
            get {
                val usuarioList = daoUsuario.allUsuarios()
                if (usuarioList.isNotEmpty()) {
                    call.respond(usuarioList)
                } else {
                    call.respondText ("No se han encontrado usuarios.")
                }
            }
            get ("/{nombre?}") {
                val username = call.parameters["nombre"] ?: return@get call.respondText(
                    "Nombre de usuario incorrecto",
                    status = HttpStatusCode.BadRequest
                )
                val usuario = daoUsuario.usuarioNombre(username) ?: return@get call.respondText(
                    "No hay usuario con este nombre",
                    status = HttpStatusCode.NotFound
                )
                call.respond(usuario)
            }
            put("/update/dinero/{idUsuario}/{dinero}"){
                val usuId: Int
                val dinero: Int
                try {
                    usuId = call.parameters["idusuario"]!!.toInt()
                    dinero = call.parameters["dinero"]!!.toInt()

                    // Actualizar idUsuario
                    if (daoUsuario.updateDineroUsuario(usuId, dinero)) {
                        call.respondText("S'ha cambiat correctament", status = HttpStatusCode.OK)
                    } else {
                        call.respondText("No s'ha pogut fer l'update", status = HttpStatusCode.InternalServerError)
                    }

                } catch (e: NumberFormatException) {
                    call.respondText("[ERROR] en el parametre.", status = HttpStatusCode.BadRequest)
                }
            }
            put("/update/password/{idUsuario}/{password}"){
                val usuId: Int
                val password: String
                try {
                    usuId = call.parameters["idusuario"]!!.toInt()
                    password = call.parameters["password"]!!

                    // Actualizar idUsuario
                    if (daoUsuario.updatePassword(usuId, password)) {
                        call.respondText("S'ha cambiat correctament", status = HttpStatusCode.OK)
                    } else {
                        call.respondText("No s'ha pogut fer l'update", status = HttpStatusCode.InternalServerError)
                    }

                } catch (e: NumberFormatException) {
                    call.respondText("[ERROR] en el parametre.", status = HttpStatusCode.BadRequest)
                }
            }

            delete("({id?})") {
                val usu_id = call.parameters["usu_id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
                val usuario = daoUsuario.usuario(usu_id.toInt()) ?: return@delete call.respondText(
                    "No hay ningun jugador con id $usu_id",
                    status = HttpStatusCode.NotFound
                )
                if (daoUsuario.deleteUsuario(usu_id.toInt())) {
                    call.respondText("Jugador borrado correctamente", status = HttpStatusCode.Accepted)
                }
            }
        }
    }
}

