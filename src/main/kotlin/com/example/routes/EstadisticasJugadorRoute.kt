package com.example.routes


import com.example.dao.daoEst
import com.example.model.EstadisticaJugador
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.estadisticasJugadorRoute() {
    route("/estadisticasJugadores"){
        get {
            val estadisticas = daoEst.allEstadisticasJugador()
            if (estadisticas.isNotEmpty()) {
                call.respond(estadisticas)
            } else {
                call.respondText("No se ha encontrado ninguna estadística de jugador", status = HttpStatusCode.OK)
            }
        }
        get("/{idJugador}") {
            val idJugador = call.parameters["idJugador"]?.toInt() ?: return@get call.respondText(
                "Falta Id",
                status = HttpStatusCode.BadRequest
            )
            val estadisticas = daoEst.allEstadisticasJugador()

            val estadisticasRespond = estadisticas.filter { it.idJugador == idJugador }

            call.respond(estadisticasRespond)
        }

    }
}

