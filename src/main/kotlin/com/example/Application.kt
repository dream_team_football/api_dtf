package com.example

import com.example.dao.DatabaseFactory
import com.example.dao.dao
import com.example.plugins.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.http.*
import io.ktor.server.plugins.cors.routing.*
import kotlinx.coroutines.runBlocking
import java.io.File
import java.net.URL

suspend fun descargarImagenes() {
    val listaJugadores = dao.allJugadores()

    for (jugador in listaJugadores) {
        val imageUrl = jugador.imagenUrl
        val imageName = jugador.nombre + ".jpg"

        try {
            val url = URL(imageUrl)
            val imageBytes = url.readBytes()

            val directorioImages = File("./images")
            directorioImages.mkdirs()

            val file = File(directorioImages, imageName)

            if (!file.exists()) {
                file.writeBytes(imageBytes)
                println("Imagen de ${jugador.nombre} descargada y guardada como $imageName")
            } else {
                println("El archivo $imageName ya existe. No se ha sobrescrito.")
            }
        } catch (e: Exception) {
            println("Error al descargar la imagen para ${jugador.nombre}: ${e.message}")
        }
    }
}
fun main() {
    embeddedServer(Netty, port = 8080, host = "192.168.1.129", module = Application::module)
        .start(wait = true)
}
// url pc Alex: 192.168.56.1
// url pc clase: 172.23.6.123 172.23.6.124
// url pc jordi: 192.168.56.1

fun Application.module() {
    DatabaseFactory.init()
    runBlocking { descargarImagenes() }
    configureSecurity()
    configureSerialization()
    configureRouting()
    install(CORS) {
        allowMethod(HttpMethod.Options)
        allowMethod(HttpMethod.Put)
        allowMethod(HttpMethod.Post)
        allowMethod(HttpMethod.Patch)
        allowMethod(HttpMethod.Delete)
        allowHeader("Content-Type")
        allowHeader("Authorization")
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }
}
