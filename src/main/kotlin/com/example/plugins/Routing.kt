package com.example.plugins

import com.example.routes.LoginRegister
import com.example.routes.estadisticasJugadorRoute
import com.example.routes.jugadorRouting
import com.example.routes.usuarioRoute
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    routing {
        usuarioRoute()
        jugadorRouting()
        LoginRegister()
        estadisticasJugadorRoute()
    }
}
