package com.example.dao

import com.example.dao.DatabaseFactory.dbQuery
import com.example.model.EstadisticaJugador
import com.example.model.EstadisticasJugador
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll

class DAOEstadisticasJugadorImpl: DAOEstadisticasJugador{

    private fun resultRowToEstadisticaJugador(row: ResultRow) = EstadisticaJugador(
        idJornada = row[EstadisticasJugador.idJornada],
        idJugador = row[EstadisticasJugador.idJugador],
        idEstadistica = row[EstadisticasJugador.idEstadistica],
        minutos = row[EstadisticasJugador.minutos],
        goles = row[EstadisticasJugador.goles],
        asistencias = row[EstadisticasJugador.asistencias],
        paradas = row[EstadisticasJugador.paradas],
        tirosAPuerta = row[EstadisticasJugador.tirosAPuerta],
        balonesPerdidos = row[EstadisticasJugador.balonesPerdidos],
        valoracion = row[EstadisticasJugador.valoracion]
    )

    override suspend fun allEstadisticasJugador(): List<EstadisticaJugador> = dbQuery {
        EstadisticasJugador.selectAll().map(::resultRowToEstadisticaJugador)
    }

    override suspend fun estadisticaJugadorId(idJugador: Int): List<EstadisticaJugador> = dbQuery {
        EstadisticasJugador
            .select { EstadisticasJugador.idJugador eq idJugador }
            .map(::resultRowToEstadisticaJugador)
    }
}

val daoEst: DAOEstadisticasJugador = DAOEstadisticasJugadorImpl().apply {  }