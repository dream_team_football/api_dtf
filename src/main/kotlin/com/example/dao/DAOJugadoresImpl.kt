package com.example.dao

import com.example.dao.DatabaseFactory.dbQuery
import com.example.model.Jugador
import com.example.model.Jugadores
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.update

class DAOJugadoresImpl: DAOJugadores{

    private fun resultToJugador(row: ResultRow) = Jugador(
        idJugador = row[Jugadores.idJugador],
        idUsuario = row[Jugadores.idUsuario],
        nombre = row[Jugadores.nombre],
        shortName = row[Jugadores.shortName],
        liga = row[Jugadores.liga],
        equipo = row[Jugadores.equipo],
        posicion = row[Jugadores.posicion],
        alineado = row[Jugadores.alineado],
        precio = row[Jugadores.precio],
        precioClausula = row[Jugadores.precioClausula],
        puntosTotales = row[Jugadores.puntosTotales],
        imagenUrl = row[Jugadores.imagenUrl]

    )

    override suspend fun allJugadores(): List<Jugador> = dbQuery {
        Jugadores.selectAll().map(::resultToJugador)
    }

    override suspend fun jugadoresName(name: String): List<Jugador> = dbQuery {
        Jugadores.select { Jugadores.nombre eq name }.map(::resultToJugador)
    }

    override suspend fun jugadoresPosicion(posicion: String): List<Jugador> = dbQuery{
        Jugadores.select(Jugadores.posicion eq posicion).map(::resultToJugador)
    }

    override suspend fun jugadoresEquipo(equipo: String): List<Jugador> = dbQuery {
        Jugadores.select(Jugadores.equipo eq equipo).map(::resultToJugador)
    }

    override suspend fun updateUsuIdJugador(idJugador: Int, usuId: Int?): Boolean = dbQuery{
        Jugadores.update({Jugadores.idJugador eq idJugador}) {
            it[idUsuario] = usuId
        } < 0

    }
    override suspend fun updateJugadorAlineado(idJugador: Int, alineado: Boolean): Boolean = dbQuery {
        Jugadores.update({Jugadores.idJugador eq idJugador}){
            it[Jugadores.alineado] = alineado
        } < 0
    }

}

val dao: DAOJugadores = DAOJugadoresImpl().apply {  }
