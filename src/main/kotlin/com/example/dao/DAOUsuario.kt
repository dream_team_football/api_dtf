package com.example.dao

import com.example.model.Usuario

interface DAOUsuario {
    suspend fun allUsuarios(): List<Usuario>
    suspend fun usuario(usu_id: Int): Usuario?
    suspend fun usuarioNombre (username: String): Usuario?
    suspend fun addNewUsuario(usu_username: String, usu_password: String): Usuario?
    suspend fun updateDineroUsuario(usu_id: Int, dinero: Int): Boolean
    suspend fun updatePassword (usu_id: Int, usu_password: String): Boolean
    suspend fun deleteUsuario(usu_id: Int): Boolean
}