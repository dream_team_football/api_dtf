package com.example.dao

import com.example.dao.DatabaseFactory.dbQuery
import com.example.model.Usuario
import com.example.model.Usuarios
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class DAOUsuarioImpl : DAOUsuario {
    private fun resultToRowUsuario (row: ResultRow) = Usuario(
        usu_id = row[Usuarios.usu_id],
        usu_username = row[Usuarios.usu_username],
        usu_password = row[Usuarios.usu_password],
        puntosTotales = row[Usuarios.puntosTotales],
        dinero = row[Usuarios.dinero]
        )

    override suspend fun allUsuarios(): List<Usuario> = dbQuery {
        Usuarios.selectAll().map(::resultToRowUsuario)
    }

    override suspend fun usuario(usu_id: Int): Usuario? = dbQuery{
        Usuarios
            .select { Usuarios.usu_id eq usu_id }
            .map(::resultToRowUsuario)
            .singleOrNull()
    }

    override suspend fun usuarioNombre(username: String): Usuario? = dbQuery{
        Usuarios
            .select { Usuarios.usu_username eq username }
            .map(::resultToRowUsuario)
            .singleOrNull()
    }

    override suspend fun addNewUsuario(usu_username: String, usu_password: String): Usuario? = dbQuery {
        val insertStatement = Usuarios.insert {
            it[Usuarios.usu_username] = usu_username
            it[Usuarios.usu_password] = usu_password
            it[puntosTotales] = 0
            it[dinero] = 30000000
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultToRowUsuario)
    }

    override suspend fun updateDineroUsuario(usu_id: Int, dinero: Int): Boolean = dbQuery {
        Usuarios.update({ Usuarios.usu_id eq usu_id}) {
            it[Usuarios.dinero] = dinero
        } < 0
    }

    override suspend fun updatePassword(usu_id: Int, usu_password: String): Boolean = dbQuery{
        Usuarios.update({Usuarios.usu_id eq usu_id}) {
            it[Usuarios.usu_password] = usu_password
        } < 0
    }

    override suspend fun deleteUsuario(usu_id: Int): Boolean = dbQuery{
        Usuarios.deleteWhere { Usuarios.usu_id eq usu_id } > 0
    }

}

val daoUsuario:DAOUsuario = DAOUsuarioImpl().apply{}