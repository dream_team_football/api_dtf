package com.example.dao

import com.example.model.EstadisticaJugador

interface DAOEstadisticasJugador {

    suspend fun allEstadisticasJugador():List<EstadisticaJugador>
    suspend fun estadisticaJugadorId(idJugador: Int): List<EstadisticaJugador>
}