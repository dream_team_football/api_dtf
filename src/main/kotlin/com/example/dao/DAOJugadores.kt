package com.example.dao

import com.example.model.Jugador

interface DAOJugadores {
    suspend fun allJugadores(): List<Jugador>
    suspend fun jugadoresName(name: String): List<Jugador>
    suspend fun jugadoresPosicion(posicion: String): List<Jugador>
    suspend fun jugadoresEquipo (equipo: String): List<Jugador>
    suspend fun updateUsuIdJugador (idJugador: Int, usuId: Int?): Boolean
    suspend fun updateJugadorAlineado (idJugador: Int, alineado: Boolean): Boolean
}